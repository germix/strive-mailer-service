
export interface StriveMailerServiceEndpointField
{
    type: 'string'|'email'|'file',                  // Tipo del campo
    name: string,                                   // Nombre del campo
    label: string,                                  // Etiqueta del campo
    mimeType?: string,                              // Sólo válido cuando 'type' tiene el valor 'file'
    noShowInTable?: boolean,                        // Indica que no se muestra en en la tabla
};

export interface StriveMailerServiceEndpoint
{
    url: string,
    title: string,
    recipient: string,
    fields: StriveMailerServiceEndpointField[],
    customData?,
}

export interface StriveMailerServiceParams
{
    serviceName: string,
    servicePort: string,
    serviceHost: string,
    corsOrigin: string,
    sendgridApiKey: string,
    senderEmailName: string,
    senderEmailAddress: string,
    template: string,
    endpoints: StriveMailerServiceEndpoint[],
}
