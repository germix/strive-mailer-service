import express from 'express'
import { Application } from 'express';
import { BadRequestError } from './error';
import { StriveMailerServiceEndpoint, StriveMailerServiceEndpointField, StriveMailerServiceParams } from './StriveMailerServiceInterfaces';
import { TableData, validateEmail } from './utils';

const cors = require('cors');
const twig = require('twig');
const multer = require('multer');
const sgMail = require('@sendgrid/mail');

export class StriveMailerService
{
    public async start(params: StriveMailerServiceParams)
    {
        const app: Application = express();

        let port: any = params.servicePort || 80;
        let hostname: any = params.serviceHost || '127.0.0.1';

        await app.listen(port, hostname);

        console.log(`${params.serviceName} started at ${hostname}:${port}`);

        app.use(cors({
            origin: params.corsOrigin || '*',
            optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
        }));
        
        app.use(express.urlencoded({
            extended: true,
        }));

        //  Allow form-data parsing
        {
            const upload = new multer({
                storage: multer.memoryStorage(),
                limits: {
                    fileSize: 6000000,
                }
            });
            app.use(upload.any());
        }

        app.get("/", function(request, response)
        {
            response.send(params.serviceName);
        });

        app.get("/status", function(request, response)
        {
            response.send('ready');
        });

        params.endpoints.forEach((endpoint) =>
        {
            let fileFields: null|StriveMailerServiceEndpointField[] = null;
            let normalFields: StriveMailerServiceEndpointField[] = [];

            // Get fields
            endpoint.fields.forEach((field) =>
            {
                if(field.type == 'file')
                {
                    if(fileFields === null)
                    {
                        fileFields = [];
                    }
                    fileFields.push(field);
                }
                else
                {
                    normalFields.push(field);
                }
            });

            // Register endpoint
            app.post(endpoint.url, async(request, response) =>
            {
                return new Promise((done, reject) =>
                {
                    // Check file fields
                    if(fileFields != null)
                    {
                        fileFields.forEach((fileField) =>
                        {
                            const found = request['files'].find((file) =>
                            {
                                return file.fieldname == fileField.name;
                            });
                            if(found === undefined)
                            {
                                throw new BadRequestError(`'${fileField.name}' is required`);
                            }
                            if(fileField.mimeType && fileField.mimeType != found.mimetype)
                            {
                                throw new BadRequestError(`'${fileField.name}' must be '${fileField.mimeType}' MIME type`);
                            }
                        })
                    }

                    // Check normal fields
                    normalFields.forEach((normalField) =>
                    {
                        const found = request.body[normalField.name];
                        if(found === undefined)
                        {
                            throw new BadRequestError(`'${normalField.name}' is required`);
                        }
                        if(normalField.type == 'email')
                        {
                            if(!validateEmail(found))
                            {
                                throw new BadRequestError(`'${normalField.name}' is not a valid email`);
                            }
                        }
                    })

                    // Make table rows
                    let rows = endpoint.fields
                    .filter((field) =>
                    {
                        return !field.noShowInTable;
                    })
                    .map((field) =>
                    {
                        return {
                            key: field.label,
                            value: field.type == 'file'
                                    ?
                                    request['files'].find((file) =>
                                    {
                                        return file.fieldname == field.name;
                                    }).originalname
                                    :
                                    request.body[field.name]
                                    ,
                        }
                    })

                    // Make attachment list
                    let attachments = fileFields == null
                        ? undefined
                        : fileFields.map((fileField) =>
                        {
                            const file = request['files'].find((file) =>
                            {
                                return file.fieldname == fileField.name;
                            });
                            return {
                                type: fileField.mimeType,
                                content: file.buffer.toString('base64'),
                                filename: file.originalname,
                                disposition: 'attachment',
                            }
                        });

                    return this.generateAndSendHtml(request, params, endpoint,
                    {
                        rows,
                        attachments,
                    })
                    .then(() =>
                    {
                        done(0);
                    })
                    .catch((error) =>
                    {
                        reject(error);
                    });
                })
                .then(() =>
                {
                    response.status(200).send({
                        ok: true,
                    });
                })
                .catch((error) =>
                {
                    let status = 0;
                    let message: any = undefined;
                    let details: any = undefined;

                    console.log(error);
                    console.log(error.response?.body);

                    if(error instanceof BadRequestError)
                    {
                        status = 400;
                        message = error.message;
                        details = error.details;
                    }
                    if(status == 0)
                    {
                        status = 500;
                        message = 'Internal error';
                    }
                    response.status(status).send({
                        ok: false,
                        error: message,
                        details: details,
                    });
                })
            })
        });
    }

    /* @internal */
    private createTable(data: TableData)
    {
        let s = '';

        s += '<div class="datatable"><table><tbody>';
        for(let i = 0; i < data.rows.length; i++)
        {
            let rowData = data.rows[i];
            let extraClass = (i < data.rows.length-1) ? 'td-border-bottom' : '';

            s += '<tr>';
                s += '<td class="first-td '+extraClass+'">'+rowData.key+':</td>';
                s += '<td class="second-td '+extraClass+'">'+rowData.value+'</td>';
            s += '</tr>';
        }
        data.rows.forEach((rowData) =>
        {
        });
        s += '</tbody></table></div>';

        return s;
    }

    /* @internal */
    private generateAndSendHtml(request, params: StriveMailerServiceParams, endpoint: StriveMailerServiceEndpoint, data: TableData)
    {
        return new Promise((done, reject) =>
        {
            twig.renderFile(params.template, {
                data: {
                    hostname: request.headers.host,
                    title: endpoint.title,
                    table: this.createTable(data),
                    ...endpoint.customData,
                }
            }, (err, html) =>
            {
                if(err)
                {
                    reject(err);
                }
                sgMail.setApiKey(params.sendgridApiKey)
                const msg =
                {
                    to: endpoint.recipient,
                    from: params.senderEmailName + '<' + params.senderEmailAddress + '>',
                    subject: endpoint.title,
                    html: html,
                    attachments: data.attachments,
                }
                sgMail
                .send(msg)
                .then(() =>
                {
                    done(0);
                })
                .catch((error) =>
                {
                    reject(error);
                })
            });
        })
    }
};
