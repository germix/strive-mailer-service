
export class BadRequestError extends Error
{
    public details;
    constructor(details)
    {
        super('Bad request');
        this.details = details;
    }
}
